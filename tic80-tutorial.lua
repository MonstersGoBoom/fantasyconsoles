
local _grv = 0.09  --  global world gravity

pi=math.pi

function cos(a)
 return math.cos(2*pi*a)
end

function sin(a)
 return -math.sin(2*pi*a)
end

add=table.insert

function del(t,a)
 for i,v in ipairs(t) do
	 if v==a then
	  t[i]=t[#t]
	  t[#t]=nil
			return
		end
	end
end

obj = {} 
obj.__index = obj
obj.list = {}    --  for our list of objects

-- our base object 
-- hooks up our _c ( constructor ) object if supplied 

function obj:new(_c)
    o = {}
    setmetatable(o, obj)

    --  randomize location and velocity
    o.x = 4+math.random(120);
    o.y = 4+math.random(120);

    o.vx = math.random(4)-2;
    o.vy = math.random(4)-2;

    --  add to global list
    --add(obj.list,o)
				table.insert(obj.list,o)

    --  construct and hookup
    -- _u is updater
    -- _r is draw / render 
    -- _d is delete 

    if _c then
        if _c._u then              -- we have a base _u
            o.on_u = _c._u         -- but more if we want
        end            
        if _c._r then	-- we don't normally use this but you might.
            o.on_r = _c._r         
        end            
        if _c._d then              -- on deletion do something
            o.on_d = _c._d  
        end
        if _c.new then              --  any further creation
            _c.new(o)               --  object specific
        end
    end

	return o
end

-- delete , call delete function , remove from list 

function obj:del()
    if self.on_d then
        self:on_d()
    end
    del(obj.list,self)
end

-- update , called every frame
function obj:_u()
    self.x = self.x + self.vx   --  apply movement 
    self.y = self.y + self.vy
    --  call updater
    if (self.on_u) then 
        self.on_u(self)
    end
end

-- if we have a draw then use it , otherwise draw a pixel
function obj:_r()
    if (self.on_r) then 
        self.on_r(self)
    else
        pix(self.x,self.y,self.color)
    end
end

--  debug function, bounce of edges of screen 
function obj:dbg()
    if (self.x<0) then 
        self.vx = -self.vx;
    end
    if (self.x>240) then 
        self.vx = -self.vx;
    end
    if (self.y<0) then 
        self.vy = -self.vy;
    end
    if (self.y>120) then 
        self.y = 120
        self.vy = -self.vy;
    end
end

---------------------------------------------------------------------
-- really simple particle , just a dot, that dies after life expires
---------------------------------------------------------------------

pr = {}
function pr:new()
    self.life = 60
    self.color = math.random(14) + 1
    self.gravity = 1.0                  -- amount of gravity applied
end
--  update , apply gravity and slowly die
function pr:_u()
    obj.dbg(self)
    self.vy = self.vy + (_grv * self.gravity); 
    if self.life~=-1 then
        self.life = self.life - 1
        if self.life == 0 then 
            self:del()
        end
    end
end

---------------------------------------------------------------------
-- simple rocket type 
-- showing how to act on deletion
-- once life runs out , spawn particles
---------------------------------------------------------------------

rkt = {}
function rkt:new()
    self.life = 32
    self.color = 7
    self.gravity = 0.01                  -- amount of gravity applied
end

-- update, slowly die, and spawn a particle every frame for "engine"" stuff

function rkt:_u()
    if self.life~=-1 then
        self.life = self.life - 1
        if self.life == 0 then 
            self:del()
        end
    end

    --  spawn new dot
    local d = obj:new(pr)
    d.x = self.x
    d.y = self.y
    d.gravity = 1.0
    d.life = 8
end

-- on delete , spawn a circle of dots
function rkt:_d()
    local oy = math.random(32);
	for ly=0,18 do
        local d = obj:new(pr)
        d.x = self.x
        d.y = self.y
        d.gravity = 1.0
        d.life = 32
        d.vx = cos((ly+oy) / 18) * 0.8
        d.vy = sin((ly+oy) / 18) * 0.8
	end
end

--  draw rocket
function rkt:_r()
    spr(3,self.x-4,self.y-4)
end
---------------------------------------------------------------------
-- mouse
---------------------------------------------------------------------
local mouse
ms = {} 
ms.bs = { off=0, clk=1, hld=2 }

function ms:new()
--    poke(0x5f2d, 1)
    self.lt = ms.bs.off
    self.rt = ms.bs.off
end



function ms:_u()
				local b=btn()
				local down
				--return(b&0x7fff)%240,(b&0x7fff)//240
    self.x = (b&0x7fff)%240
    self.y = (b&0x7fff)//240
    --  really simply logic to help with button debounce
    --  we can check ms.lt==ms.bs.hld 
    --  or ms.bs.clk if we let go , for example autofire or only a full press

    if self.lt~=ms.bs.off then
      self.lt=self.lt-1
    end
    if b>>15 then
        self.lt = ms.bs.hld
    end
end

function ms:_r()
    spr(1,self.x,self.y)
end

-- demo hero, buttons and mouse controls 
local player
plr = {}
function plr:new()
    self.x = 64
    self.y = 120
    self.vx = 0
    self.vy = 0
end
-- 
function plr:_u()
    --  slow down over time
    self.vx = self.vx * 0.96
    --  bounce off walls
    obj.dbg(self)

    --  controls
    if btn(2) then
        self.vx = self.vx - 0.1
    elseif btn(3) then
        self.vx = self.vx + 0.1
    end
    --  clamp speed 
    if self.vx<-2.5 then self.vx = -2.5 end
    if self.vx>2.5 then self.vx = 2.5 end

    --  if mouse is clicked then launch rkt 

    if btnp(4) then
        local d = obj:new(rkt)
        d.x = self.x
        d.y = self.y
        d.vx = (mouse.x - self.x)/64
        d.vy = (mouse.y - self.y)/64
        d.life = 64
        d.gravity = 0.01
    end
end

function plr:_r()
    spr(2,self.x,self.y)
end


function _init( )
    mouse = obj:new(ms)
    player = obj:new(plr)
end

function _update60()
    for pv,p in pairs(obj.list) do
        p:_u();
    end
end

function _draw()
    cls();
    for pv,p in pairs(obj.list) do
        p:_r()
    end
end

_init()

function TIC()
	_update60()
	_draw()
end
